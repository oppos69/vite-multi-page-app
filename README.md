
# 需求
在一般的开发中我们经常遇到编写一些非常简单的页面，比如活动页，落地页等等。这种页面要求第一诉求就是速度，所以用jQuery、vue、react 都有点臃肿，但是又需要用到一些前端工程上一些功能，比如css自动添加浏览器前缀、移动端适配、热重载等功能。于是就想用vite来做。

# 创建流程
* 使用vite创建模板

```
yarn create vite my-vue-app --template vanilla
```
* 添加依赖

```
# 移动端适配方面的库
yarn add autoprefixer -D
yarn add postcss-px-to-viewport -D
# 如果运行报错需要进入node_modules postcss-px-to-viewport/index.js 修改atRule 为 AtRule
yarn add normalize.css

```
* 修改vite.config.js 配置

```
const { resolve } = require('path')
const { defineConfig } = require('vite')

module.exports = defineConfig({
  build: {
    rollupOptions: {
      input: {
        main: resolve(__dirname, 'index.html'),
        nested: resolve(__dirname, 'nested')
      }
    }
  },
  css: {
    modules: {
      localsConvention: 'camelCaseOnly'
    },
    postcss: {
      plugins: [
        require("autoprefixer"),
        require("postcss-px-to-viewport")({
          unitToConvert: 'px',
          viewportWidth: 375,
          unitPrecision: 3,
          propList: ['*'],
          viewportUnit: 'vw',
          fontViewportUnit: 'vw',
          selectorBlackList: ['ignore-'],
          minPixelValue: 1,
          mediaQuery: false,
          replace: true,
          exclude: [],
          landscape: true,
          landscapeUnit: 'vw',
          landscapeWidth: 750,
        })
      ]
    }
  }
})
```

* 添加ajax请求
  
/utils/ajax.js

# 仓库地址

[vite-multi-page-app](https://gitee.com/oppos69/vite-multi-page-app)