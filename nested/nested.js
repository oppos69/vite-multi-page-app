import classes from '../css/color.module.css'
import nested from './nested.css'
import 'normalize.css'
import { ajax_method_get, ajax_method_post } from '../utils/ajax'

document.getElementById("app").className = classes.primaryColor

// get 请求
ajax_method_get('http://httpbin.org/get', {a: '1'}, (response) => {
  console.log(response)
})

// post 请求
ajax_method_post("http://httpbin.org/post", { b: '2'}, (response) => {
  console.log(response)
})