export function ajax_method_get(url,data,success, responseType = 'json'){

  var r = new XMLHttpRequest();
  r.responseType = responseType;
  const paramsArr = [];
  if (typeof data == 'object'){
    
    for (const key in data) {
      if (data.hasOwnProperty.call(data, key)) {
        const element = data[key];
        paramsArr.push(key + "=" + element)
      }
    }
  }
  const paramsStr = paramsArr.join("&")

  r.open("GET", url + "?" + paramsStr, true);
  r.onreadystatechange = function () {
    if (r.readyState != 4 || r.status != 200) return;
    if (r.responseType == 'json'){
      success(r.response)
    }
    if (r.responseType == 'text'){
      success(r.responseText)
    }
    
    //success(r.responseText)
  };
  
  r.send();
}


export function ajax_method_post(url,data,success, responseType = 'json'){

  var r = new XMLHttpRequest();
  r.responseType = responseType;
  r.open("POST", url, true);
  r.setRequestHeader('Accept', 'application/json'); 
  r.onreadystatechange = function () {
    if (r.readyState != 4 || r.status != 200) return;
    if (r.responseType == 'json'){
      success(r.response)
    }
    if (r.responseType == 'text'){
      success(r.responseText)
    }
  };
  r.send(JSON.stringify(data));
}
